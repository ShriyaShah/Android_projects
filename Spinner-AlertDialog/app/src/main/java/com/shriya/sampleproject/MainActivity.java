package com.shriya.sampleproject;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {
    LinearLayout linearLayout;
    Spinner spinner;
    ArrayAdapter<CharSequence> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        linearLayout = (LinearLayout) findViewById(R.id.linear_layout);
        spinner = (Spinner) findViewById(R.id.spinner);
        adapter = ArrayAdapter.createFromResource(this,R.array.color,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            AlertDialog.Builder a_builder = new AlertDialog.Builder(MainActivity.this);
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, final int position, long l) {
                if(adapterView.getItemAtPosition(position).equals("")){ }
                else{
                a_builder.setMessage("Do you want to change background to " + adapterView.getItemAtPosition(position))
                        .setCancelable(false)
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.cancel();
                                    }
                                }
                        )
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                switch (position)
                {
                    case 1:
                        linearLayout.setBackgroundColor(getResources().getColor(R.color.NoBackground));
                        break;
                    case 2:
                        linearLayout.setBackgroundColor(getResources().getColor(R.color.Purple));
                        break;
                    case 3:
                        linearLayout.setBackgroundColor(getResources().getColor(R.color.Cyan));
                        break;
                    case 4:
                        linearLayout.setBackgroundColor(getResources().getColor(R.color.Blue));
                        break;
                    case 5:
                        linearLayout.setBackgroundColor(getResources().getColor(R.color.Pink));
                        break;
                    case 6:
                        linearLayout.setBackgroundColor(getResources().getColor(R.color.Orange));
                        break;
                    case 7:
                        linearLayout.setBackgroundColor(getResources().getColor(R.color.Yellow));
                        break;
                }
                    }
                });

                AlertDialog alertDialog = a_builder.create();
                alertDialog.setTitle("Alert!");
                alertDialog.show();
            }}


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
