package com.shriya.courtcounter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView tvScoreA,tvScoreB;
    Button twoA,twoB,threeA,threeB,freeBasketA,freeBasketB,reset,finish;
    int scoreA=0,scoreB=0;
    String result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvScoreA = (TextView) findViewById(R.id.tv_score_a);
        tvScoreB = (TextView) findViewById(R.id.tv_score_b);
        twoA = (Button) findViewById(R.id.plus_two_a);
        twoB = (Button) findViewById(R.id.plus_two_b);
        threeA = (Button) findViewById(R.id.plus_three_a);
        threeB = (Button) findViewById(R.id.plus_three_b);
        freeBasketA = (Button) findViewById(R.id.free_basket_a);
        freeBasketB = (Button) findViewById(R.id.free_basket_b);
        reset = (Button) findViewById(R.id.reset);
        finish = (Button) findViewById(R.id.finish);

        scoreA = Integer.parseInt(tvScoreA.getText().toString());
        scoreB = Integer.parseInt(tvScoreB.getText().toString());

        twoA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scoreA = scoreA + 2;
                tvScoreA.setText("" + scoreA);
            }
        });

        twoB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scoreB = scoreB + 2;
                tvScoreB.setText("" + scoreB);
            }
        });

        threeA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scoreA = scoreA + 3;
                tvScoreA.setText("" + scoreA);
            }
        });

        threeB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scoreB = scoreB + 3;
                tvScoreB.setText("" + scoreB);
            }
        });

        freeBasketA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scoreA = scoreA + 1;
                tvScoreA.setText("" + scoreA);
            }
        });

        freeBasketB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scoreB = scoreB + 1;
                tvScoreB.setText("" + scoreB);
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scoreA = 0;
                scoreB = 0;
                tvScoreA.setText("" + scoreA);
                tvScoreB.setText("" + scoreB);
            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(scoreA>scoreB)
                {
                    Toast.makeText(getApplicationContext(), "Team A wins", Toast.LENGTH_LONG).show();
                    result = "Team A Wins";
                }
                if(scoreA<scoreB)
                {
                    Toast.makeText(getApplicationContext(), "Team B wins", Toast.LENGTH_LONG).show();
                    result = "Team B Wins";
                }
                if(scoreA == scoreB){
                    Toast.makeText(getApplicationContext(), "Match draw", Toast.LENGTH_LONG).show();
                    result = "Match Draw";
                }
                Intent intent = new Intent(getBaseContext(), resultActivity.class);
                intent.putExtra("Match result", result);
                startActivity(intent);



            }
        });
    }
}
