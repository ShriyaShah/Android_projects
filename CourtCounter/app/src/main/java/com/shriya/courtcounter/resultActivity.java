package com.shriya.courtcounter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.widget.Button;
import android.widget.TextView;

public class resultActivity extends AppCompatActivity {
    TextView ResultTv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        ResultTv=findViewById(R.id.tv_Result);
        String result = getIntent().getStringExtra("Match result");
//        ResultTv.setText(result);

        SpannableString content = new SpannableString(result);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        ResultTv.setText(content);

    }
}
