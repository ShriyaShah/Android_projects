package com.shriya.fragment_activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class fragment_one extends Fragment implements View.OnClickListener {
    EditText name;
    Button submit;
    private OnFragmentInteractionListener mListener;
    public fragment_one() {}

     @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_one, container, false);
         name = (EditText) view.findViewById(R.id.ev_name) ;
         submit = (Button) view.findViewById(R.id.bt_frag_one);
         submit.setOnClickListener(this);
         return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.bt_frag_one:
                String n = name.getText().toString();
                mListener.onFragmentInteraction(n);
        }
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(String name);
    }
}
