package com.shriya.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String CUSTOM_INTENT = "my.custom.action.tag.fordemo";
    Button buttonBroadcastIntent;
    EditText Name;

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(getApplicationContext(), "Broadcast received", Toast.LENGTH_LONG).show();

            if (intent.getBooleanExtra("state", false))
                changeImage(R.drawable.ic_airplanemode_active_black_24dp);
            else
                changeImage(R.drawable.ic_airplanemode_inactive_black_24dp);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonBroadcastIntent = (Button) findViewById(R.id.button);
        Name = (EditText) findViewById(R.id.et_name);



                buttonBroadcastIntent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(CUSTOM_INTENT);
                Bundle name = new Bundle();
                name.putString("Name", Name.getText().toString());
                intent.putExtras(name);
                sendBroadcast(intent);
            }
        });

    }

    private void changeImage(int image) {
        ImageView i =(ImageView) findViewById(R.id.image);
        i.setImageResource(image);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        registerReceiver(new MyReceiver(),
//                new IntentFilter(CUSTOM_INTENT));
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter=new IntentFilter();
        intentFilter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        registerReceiver(receiver,intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //unregisterReceiver(receiver);
    }


}