package com.shriya.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

/**
 * Created by shriya on 29/12/17.
 */

public class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Bundle extras = intent.getExtras();
        String value = null;
        if(extras != null)
        {
        value = extras.getString("Name");}
        Toast.makeText(context, "Broadcast received in MyReceiver" + " Name is : " + value , Toast.LENGTH_LONG).show();
    }
}
