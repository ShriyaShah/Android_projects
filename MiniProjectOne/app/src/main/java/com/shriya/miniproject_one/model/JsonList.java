package com.shriya.miniproject_one.model;

public class JsonList {

    private String mId,mName, mMobile, mCity, mImageUri, mDate, mCompany, mProfessional, mGender;

    public JsonList(String id,String name,String mobile,String city,String imageUri,String date,String company,String professional,String gender) {
        mId = id;
        mName = name;
        mMobile = mobile;
        mCity = city;
        mImageUri = imageUri;
        mDate = date;
        mCompany = company;
        mProfessional = professional;
        mGender = gender;
    }
    public String getmId() {
        return mId;
    }

    public String getmName() {
        return mName;
    }

    public String getmMobile() {
        return mMobile;
    }

    public String getmCity() {
        return mCity;
    }

    public String getmImageUri() { return mImageUri; }

    public String getmDate() {  return mDate; }

    public String getmCompany() {
        return mCompany;
    }

    public String getmProfessional() { return mProfessional; }

    public String getmGender() {  return mGender; }
}
