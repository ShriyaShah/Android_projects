package com.shriya.miniproject_one.activity;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.shriya.miniproject_one.R;

public class DetailsActivity extends AppCompatActivity {
    ImageView image;
    TextView id,name,mobile,city,date,company,professional,gender;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        image = (ImageView) findViewById(R.id.iv_i_image);
        id = (TextView) findViewById(R.id.tv_i_id);
        name = (TextView) findViewById(R.id.tv_i_name);
        mobile = (TextView) findViewById(R.id.tv_i_mobile);
        city = (TextView) findViewById(R.id.tv_i_city);
        date = (TextView) findViewById(R.id.tv_i_date);
        company = (TextView) findViewById(R.id.tv_i_company);
        professional = (TextView) findViewById(R.id.tv_i_professional);
        gender = (TextView) findViewById(R.id.tv_i_gender);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
                if(extras.getString("mimage") != "")
                Glide.with(this)
                        .load(extras.getString("mimage"))
                        .apply(new RequestOptions().placeholder(R.drawable.list_background).error(R.drawable.list_background))
                        .into(image);
            id.setText(extras.getString("mid"));
            name.setText(extras.getString("mname"));
            mobile.setText(extras.getString("mmobile"));
            city.setText(extras.getString("mcity"));
            date.setText(extras.getString("mdate"));
            company.setText(extras.getString("mcompany"));
            professional.setText(extras.getString("mprofessional"));
            gender.setText(extras.getString("mgender"));
        }
    }
}
