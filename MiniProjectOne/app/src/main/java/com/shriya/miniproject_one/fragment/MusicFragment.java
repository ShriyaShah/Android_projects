package com.shriya.miniproject_one.fragment;

import android.content.Context;
import android.os.Bundle;
    import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.shriya.miniproject_one.adapter.MusicAdapter;
import com.shriya.miniproject_one.model.MusicContent;
import com.shriya.miniproject_one.R;

import java.util.ArrayList;
/**
 * A simple {@link Fragment} subclass.
 */
public class MusicFragment extends Fragment {
    RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_music, container, false);// Inflate the layout for this fragment

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(),new LinearLayoutManager(getActivity()).getOrientation());
        final ArrayList<MusicContent> content = new ArrayList<MusicContent>();
        content.add(new MusicContent("Song 1","This is first song",  R.drawable.launch_screen, R.raw.song));
        content.add(new MusicContent("Song 2","This is second song",  R.drawable.launch_screen, R.raw.song));
        content.add(new MusicContent("Song 3","This is third song",  R.drawable.launch_screen, R.raw.song));
        content.add(new MusicContent("Song 4","This is fourth song",  R.drawable.launch_screen, R.raw.song));
        content.add(new MusicContent("Song 5","This is fifth song",  R.drawable.launch_screen, R.raw.song));
        content.add(new MusicContent("Song 6","This is sixth song",  R.drawable.launch_screen, R.raw.song));
        content.add(new MusicContent("Song 7","This is seventh song",  R.drawable.launch_screen, R.raw.song));
        content.add(new MusicContent("Song 8","This is eighth song",  R.drawable.launch_screen, R.raw.song));

        recyclerView = (RecyclerView) rootView.findViewById(R.id.music);
        MusicAdapter adapter = new MusicAdapter(getActivity(),R.layout.list_item, content);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(adapter);
        return rootView;
    }

}
