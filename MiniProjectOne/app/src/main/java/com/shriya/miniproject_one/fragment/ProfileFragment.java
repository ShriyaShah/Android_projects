package com.shriya.miniproject_one.fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;


import com.bumptech.glide.Glide;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.shriya.miniproject_one.R;
import com.shriya.miniproject_one.activity.CategoryActivity;
import com.shriya.miniproject_one.activity.LoginActivity;

import java.io.InputStream;

import static android.content.Intent.getIntent;
import static android.content.Intent.getIntentOld;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    View rootview;
    public static Boolean FLAG_BUTTON= false;
    TextView nameView, emailView;
    ImageView imageView;
    String type,name,surname,NAME = "", URL = "",typef;
    LinearLayout ll_name,ll_email,ll_date,ll_gender;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FacebookSdk.sdkInitialize(getActivity());
        rootview = inflater.inflate(R.layout.fragment_profile, container, false);

        ll_name = (LinearLayout) rootview.findViewById(R.id.ll_profile_name);
        ll_email = (LinearLayout) rootview.findViewById(R.id.ll_profile_email);
        ll_date = (LinearLayout) rootview.findViewById(R.id.ll_birthdate);
        ll_gender = (LinearLayout) rootview.findViewById(R.id.ll_gender);
        imageView = (ImageView) rootview.findViewById(R.id.iv_profile_photo);
        FLAG_BUTTON= true;

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("Data",Context.MODE_PRIVATE);
        name = sharedPreferences.getString("name", "");
        surname = sharedPreferences.getString("surname", "");
        URL = sharedPreferences.getString("ProfileImageUrl", "");
        type = sharedPreferences.getString("type","");
        typef = sharedPreferences.getString("typef", "");
        NAME = "" + name + " " + surname;

        if(name != "")
        {   nameView = (TextView) rootview.findViewById(R.id.tv_profile_name);
            ll_name.setVisibility(View.VISIBLE);
            nameView.setText(type != "" ? name : typef != "" ? NAME : name);}
        if(type != "") {
            if (surname != "") {
                emailView = (TextView) rootview.findViewById(R.id.tv_profile_email);
                ll_email.setVisibility(View.VISIBLE);
                emailView.setText(surname);
            }
        }

        if (URL != "")
        {Glide.with(this)
                .load(URL)
                .into(imageView);}

        android.support.v7.widget.Toolbar toolbarName = (android.support.v7.widget.Toolbar) rootview.findViewById(R.id.profile_toolbar);
        toolbarName.setTitle(type != "" ? name : typef != "" ? NAME : name);

        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) rootview.findViewById(R.id.collapsingToolbar);
        collapsingToolbar.setTitle(type != "" ? name : typef != "" ? NAME : name);

        Button logout = (Button) rootview.findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref = getActivity().getSharedPreferences("Data",Context.MODE_PRIVATE);
                pref.getString("Flag", "");
                if(Integer.parseInt(pref.getString("Flag", "0")) == 1)
                {   LoginManager.getInstance().logOut();
                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences("Data", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.clear();
                    editor.commit();
                    FLAG_BUTTON = false;
                }
                else
                    FLAG_BUTTON = true;
                getActivity().finish();
                Intent login = new Intent(getActivity(), LoginActivity.class);
                startActivity(login);
            }
        });
        return rootview;
    }

}
