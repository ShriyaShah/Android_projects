package com.shriya.miniproject_one.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.shriya.miniproject_one.R;
import com.shriya.miniproject_one.fragment.ListFragment;
import com.shriya.miniproject_one.model.AnimationUtil;
import com.shriya.miniproject_one.model.JsonList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by shriya on 10/1/18.
 */

public class JsonListAdapter extends RecyclerView.Adapter<JsonListAdapter.MyViewHolder> {

    private Context context;
    List<JsonList> list = Collections.emptyList();
    private static final int LIST_ITEM = 0;
    private static final int GRID_ITEM = 1;
    boolean isSwitchView = true;
    private int previousPosition = 0;
    private ListFragment.OnFragmentInteractionListener mListener;

    public JsonListAdapter(Context activity, List<JsonList> jsonLists) {

        this.context = activity;
        list = jsonLists;
    }

    @Override
    public JsonListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(i == LIST_ITEM ?  R.layout.json_list_item_list : R.layout.json_list_item_grid, null);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, final int position) {

        final JsonList current = list.get(position);
        myViewHolder.NameTextView.setText(current.getmName());
        myViewHolder.CityTextView.setText(current.getmCity());
        if (current.getmImageUri() != "") {
                Glide.with(context)
                        .load(current.getmImageUri())
                        .apply(new RequestOptions().placeholder(R.drawable.list_background).error(R.drawable.list_background))
                        .into(myViewHolder.ImageView);
                }

        if(position > previousPosition){ // We are scrolling DOWN

            AnimationUtil.animate(myViewHolder, true);

        }else{ // We are scrolling UP

            AnimationUtil.animate(myViewHolder, false);

        }

        previousPosition = position;
        final JsonList currentEarthquake = list.get(position);


        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(context, "OnClick Called at position " + position, Toast.LENGTH_SHORT).show();
                if (context instanceof ListFragment.OnFragmentInteractionListener) {
                    mListener = (ListFragment.OnFragmentInteractionListener) context;
                } else {
                    throw new RuntimeException(context.toString()
                            + " must implement OnFragmentInteractionListener");
                }
                    String Id = currentEarthquake.getmId();
                    String name = currentEarthquake.getmName();
                    String mobile = currentEarthquake.getmMobile();
                    String city = currentEarthquake.getmCity();
                    String date = currentEarthquake.getmDate();
                    String company = currentEarthquake.getmCompany();
                    String professional = currentEarthquake.getmProfessional();
                    String gender = currentEarthquake.getmGender();
                    String image = currentEarthquake.getmImageUri();

                    mListener.onFragmentInteraction(image, Id, name, mobile, city, date, company, professional, gender);
                 }
        });

        myViewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                Toast.makeText(context, "OnLongClick Called at position " + position, Toast.LENGTH_SHORT).show();
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Close");
                builder.setMessage("Do you want to delete");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {}
                });
                builder.setNeutralButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();

                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                return true;
                }
        });
    }

    @Override
    public int getItemViewType (int position) {
        if (isSwitchView){
            return LIST_ITEM;
        }else{
            return GRID_ITEM;
        }
    }

    public boolean toggleItemViewType () {
        isSwitchView = !isSwitchView;
        return isSwitchView;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView NameTextView,CityTextView;
        ImageView ImageView;
        public MyViewHolder(View itemView) {
            super(itemView);
            NameTextView = (TextView) itemView.findViewById(R.id.tv_json_name);
            CityTextView = (TextView) itemView.findViewById(R.id.tv_json_city);
            ImageView = (ImageView) itemView.findViewById(R.id.iv_json_image);
        }
    }

}
