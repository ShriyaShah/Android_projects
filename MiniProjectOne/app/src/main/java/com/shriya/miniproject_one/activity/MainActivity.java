package com.shriya.miniproject_one.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.shriya.miniproject_one.R;

public class MainActivity extends AppCompatActivity {
    private int SLEEP_TIMER = 2;
    private ImageView impsplashicon;
    private TextView title;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        //getSupportActionBar().hide();
        LogoLauncher logoLauncher = new LogoLauncher();
        logoLauncher.start();

        impsplashicon = findViewById(R.id.im_simlie);
        title = findViewById(R.id.tv_title);
        Animation myanim = AnimationUtils.loadAnimation(this, R.anim.mytransition);
        impsplashicon.startAnimation(myanim);
        title.startAnimation(myanim);

    }

    private class LogoLauncher extends Thread {
        public void run() {
            try {
                sleep(1000 * SLEEP_TIMER);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            SharedPreferences sharedPreferences = getSharedPreferences("Data",MODE_PRIVATE);
            String googleId = sharedPreferences.getString("googleId", null);
            String facebookId = sharedPreferences.getString("facebookId", null);
            if(googleId != null || facebookId != null)
               intent = new Intent(MainActivity.this, CategoryActivity.class);
            else
                intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
            MainActivity.this.finish();
        }


    }
}
