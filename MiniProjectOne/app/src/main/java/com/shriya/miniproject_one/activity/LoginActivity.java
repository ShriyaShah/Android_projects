package com.shriya.miniproject_one.activity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
//import android.app.Fragment;
import android.support.v4.app.Fragment;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.shriya.miniproject_one.R;
import com.shriya.miniproject_one.fragment.ProfileFragment;

import static android.text.TextUtils.isEmpty;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    public static SignInButton signInButton;
    LoginButton loginButton;
    CallbackManager callbackManager;
    public static GoogleApiClient googleApiClient;
    View v;
    Button signOut, button_continue;
    public int FLAG = 0;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    public static final int REQ_CODE = 9001;
    public static String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);
        signInButton = (SignInButton) findViewById(R.id.button_google);
        loginButton = (LoginButton) findViewById(R.id.button_facebook);
        signOut = (Button) findViewById(R.id.signout_google);
        button_continue = (Button) findViewById(R.id.continue_google);
        button_continue.setOnClickListener(this);
        if (ProfileFragment.FLAG_BUTTON == true) {
            ProfileFragment.FLAG_BUTTON = false;
            signInButton.setVisibility(View.GONE);
            signOut.setVisibility(View.VISIBLE);
            button_continue.setVisibility(View.VISIBLE);
        }
        signOut.setOnClickListener(this);
        v = findViewById(android.R.id.content);

        loginButton.setReadPermissions("email", "public_profile");

        ConnectivityManager conMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        //mobile
        NetworkInfo.State mobile = conMan.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState();
        //wifi
        NetworkInfo.State wifi = conMan.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState();
        if (mobile == NetworkInfo.State.CONNECTED || mobile == NetworkInfo.State.CONNECTING) {
            Snackbar.make(v, "Connected on mobile data", Snackbar.LENGTH_LONG).show();
        } else if (wifi == NetworkInfo.State.CONNECTED || wifi == NetworkInfo.State.CONNECTING) {
            Snackbar.make(v, "Connected on wifi", Snackbar.LENGTH_LONG).show();
        } else {
            Snackbar.make(v, "No Internet Connection", Snackbar.LENGTH_LONG).show();
        }
        callbackManager = CallbackManager.Factory.create();
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {
            }
        };
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                nextActivity(newProfile);
            }
        };
        accessTokenTracker.startTracking();
   //     profileTracker.startTracking();

        signInButton.setOnClickListener(this);
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions).build();
        FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                onClick(signOut);

                Profile profile = Profile.getCurrentProfile();
                if (profile == null) {
                    profileTracker.startTracking();
                } else {
                    nextActivity(profile);
                }
                Toast.makeText(getApplicationContext(), "Logging in...", Toast.LENGTH_SHORT).show();
                LoginActivity.this.finish();
                Intent intent = new Intent(LoginActivity.this, CategoryActivity.class);
                LoginActivity.this.startActivity(intent);

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }

        };

        loginButton.registerCallback(callbackManager, callback);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    protected void onStop() {
        super.onStop();
        //Facebook login
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_google:
                signIn();
                break;
            case R.id.signout_google:
                Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        SharedPreferences sharedPreferences = getSharedPreferences("Data", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.clear();
                        editor.commit();

                        signInButton.setVisibility(View.VISIBLE);
                        signOut.setVisibility(View.GONE);
                        button_continue.setVisibility(View.GONE);
                    }
                });
                break;
            case R.id.continue_google:
                Intent intent = new Intent(this, CategoryActivity.class);
                startActivity(intent);
                break;
        }
    }

    public void signIn() {
        LoginManager.getInstance().logOut();
        FLAG = 0;
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, REQ_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE) {
            signInButton.setVisibility(View.GONE);
            signOut.setVisibility(View.VISIBLE);
            button_continue.setVisibility(View.VISIBLE);
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);


            if (resultCode != 0) {
                GoogleSignInAccount account = result.getSignInAccount();
                SharedPreferences sharedPreferences = getSharedPreferences("Data", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("type", "google");
                editor.putString("googleId", account.getId());
                editor.putString("name", account.getDisplayName());
                editor.putString("surname", account.getEmail());
                editor.putString("ProfileImageUrl", !Uri.EMPTY.equals(account.getPhotoUrl()) ? account.getPhotoUrl().toString() : "");
                editor.commit();

                Intent intent = new Intent(this, CategoryActivity.class);
                LoginActivity.this.startActivity(intent);
                LoginActivity.this.finish();
            } else {
                Intent intent = new Intent(this, LoginActivity.class);
                LoginActivity.this.startActivity(intent);
            }
        }

        LoginActivity.this.finish();
    }

    private void nextActivity(Profile profile) {
        if (profile != null) {
            FLAG = 1;
            SharedPreferences sharedPreferences = getSharedPreferences("Data", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("typef", "facebook");
            editor.putString("facebookId", profile.getId());
            editor.putString("name", profile.getFirstName());
            editor.putString("surname", profile.getLastName());
            editor.putString("ProfileImageUrl", !Uri.EMPTY.equals(profile.getProfilePictureUri(150, 150).toString()) ? profile.getProfilePictureUri(150, 150).toString() : "");
            editor.putString("Flag", String.valueOf(FLAG));
            editor.commit();
        }

    }
}