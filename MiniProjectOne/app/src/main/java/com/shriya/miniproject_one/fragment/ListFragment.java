package com.shriya.miniproject_one.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.shriya.miniproject_one.R;
import com.shriya.miniproject_one.adapter.JsonListAdapter;
import com.shriya.miniproject_one.model.JsonList;
import com.shriya.miniproject_one.model.QueryUtils;

import java.util.ArrayList;
import java.util.List;

public class ListFragment extends Fragment {

    private static final String USGS_REQUEST_URL = "http://iwebworld.info/jsontest/getData";
    View rootview;
    List<JsonList> result;
    ArrayList<JsonList> Data = new ArrayList<>();
    FloatingActionButton fab;
    RecyclerView recyclerView;
    int scrollPosition = 0;
    private JsonListAdapter mListAdapter;
    private OnFragmentInteractionListener mListener;

    public ListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_list, container, false);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(), new LinearLayoutManager(getActivity()).getOrientation());

        fab = (FloatingActionButton) rootview.findViewById(R.id.fab);
        recyclerView = (RecyclerView) rootview.findViewById(R.id.recyclerview);
        mListAdapter = new JsonListAdapter(getActivity(), Data);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mListAdapter);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (recyclerView.getLayoutManager() != null) {
                    scrollPosition = ((LinearLayoutManager) recyclerView.getLayoutManager())
                            .findFirstCompletelyVisibleItemPosition();
                }
                boolean isSwitched = mListAdapter.toggleItemViewType();
                fab.setImageResource(isSwitched ? R.drawable.ic_grid_on_black_24dp : R.drawable.ic_list_black_24dp_tab);
                recyclerView.setLayoutManager(isSwitched ? new LinearLayoutManager(getActivity()) : new GridLayoutManager(getActivity(), 2));
                mListAdapter.notifyDataSetChanged();
                recyclerView.scrollToPosition(scrollPosition);
            }
        });

        ListAsyncTask task = new ListAsyncTask();
        task.execute(USGS_REQUEST_URL);

        return rootview;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(String image, String id, String name, String mobile, String city, String date, String company, String professional, String gender);
    }

    private class ListAsyncTask extends AsyncTask<String, Void, ArrayList<JsonList>> {

        @Override
        protected ArrayList<JsonList> doInBackground(String... urls) {
            if (urls.length < 1 || urls[0] == null) {
                return null;
            }
            result = QueryUtils.fetchListData(urls[0]);


            return (ArrayList<JsonList>) result;
        }

        @Override
        protected void onPostExecute(ArrayList<JsonList> data) {
            Data.addAll(data);
            mListAdapter.notifyDataSetChanged();
        }
    }
}
