package com.shriya.miniproject_one.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.shriya.miniproject_one.model.MusicContent;
import com.shriya.miniproject_one.R;

import java.util.Collections;
import java.util.List;

public class MusicAdapter extends RecyclerView.Adapter<MusicAdapter.MyViewHolder>{
    private Context context;
    private MediaPlayer mMediaPlayer;
    private List<MusicContent> arrayList = Collections.emptyList();;
    private AudioManager mAudioManager;
    public Boolean flag = true;
    private AudioManager.OnAudioFocusChangeListener mOnAudioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int focusChange) {
            if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT ||
                    focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK && mMediaPlayer != null) {
                mMediaPlayer.pause();
                mMediaPlayer.seekTo(0);
            } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN && mMediaPlayer != null) {
                mMediaPlayer.start();
            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                releaseMediaPlayer();
            }
        }
    };
    private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            releaseMediaPlayer();
        }
    };

    public MusicAdapter(@NonNull Context context, int resource, @NonNull List<MusicContent> objects) {
      //  super(context, 0, objects);
        arrayList = objects;
        this.context = context;

    }

//    @Override
//    public View getView(final int position, View convertView, ViewGroup parent) {
//
//        View listItemView = convertView;
//        if (listItemView == null) {
//            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
//        }
//        mAudioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
//        final MusicContent currentNumber = getItem(position);
//        TextView nameTextView = (TextView) listItemView.findViewById(R.id.tv_name);
//        nameTextView.setText(currentNumber.getSongName());
//        nameTextView.setTypeface(null, Typeface.BOLD_ITALIC);
//        TextView singerTextView = (TextView) listItemView.findViewById(R.id.tv_singer);
//        singerTextView.setText(currentNumber.getSinger());
//        singerTextView.setTypeface(null, Typeface.ITALIC);
//        ImageView image = (ImageView) listItemView.findViewById(R.id.iv_one);
//        image.setImageResource(currentNumber.getImageResourceId());
//        final ImageView playPause = (ImageView) listItemView.findViewById(R.id.iv_play_pause);
//        ImageView stop = (ImageView) listItemView.findViewById(R.id.iv_stop);
//        playPause.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //releaseMediaPlayer();
//
//                MusicContent word = arrayList.get(position);
//                int result = mAudioManager.requestAudioFocus(mOnAudioFocusChangeListener,
//                        AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
//                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
//                    if(flag){
//                        mMediaPlayer = MediaPlayer.create(context, word.getSongResourceId());
//                        flag = false;
//                    }
//                    if(mMediaPlayer != null && mMediaPlayer.isPlaying()) {
//                        mMediaPlayer.pause();
//                        currentNumber.setmTime(mMediaPlayer.getCurrentPosition());
//                        playPause.setImageResource(R.drawable.ic_play_arrow_black_24dp);
//                    } else if(mMediaPlayer != null){
//                        mMediaPlayer = MediaPlayer.create(context, word.getSongResourceId());
//                        mMediaPlayer.seekTo((int)currentNumber.getmTime());
//                        mMediaPlayer.start();
//                        mMediaPlayer.setOnCompletionListener(mCompletionListener);
//                        playPause.setImageResource(R.drawable.ic_pause_black_24dp);
//                    }
//                }
//            }
//        });
//
//        stop.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(!flag && mMediaPlayer != null) {
//                    mMediaPlayer.stop();
//                    mMediaPlayer.release();
//                    flag = true;
//                }
//                playPause.setImageResource(R.drawable.ic_play_arrow_black_24dp);
//            }
//        });
//        return listItemView;
//    }

    private void releaseMediaPlayer() {
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
            mAudioManager.abandonAudioFocus(mOnAudioFocusChangeListener);
        }
    }

    @Override
    public MusicAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        MyViewHolder holder = new MyViewHolder(listItemView);
        return holder;
    }


    @Override
    public void onBindViewHolder(final MusicAdapter.MyViewHolder holder, final int position) {
        final MusicContent currentNumber = arrayList.get(position);
        mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        holder.nameTextView.setText(currentNumber.getSongName());
        holder.nameTextView.setTypeface(null, Typeface.BOLD_ITALIC);
        holder.singerTextView.setText(currentNumber.getSinger());
        holder.singerTextView.setTypeface(null, Typeface.ITALIC);
        holder.image.setImageResource(currentNumber.getImageResourceId());

        holder.playPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //releaseMediaPlayer();

                MusicContent word = arrayList.get(position);
                int result = mAudioManager.requestAudioFocus(mOnAudioFocusChangeListener,
                        AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    if(flag){
                        mMediaPlayer = MediaPlayer.create(context, word.getSongResourceId());
                        flag = false;
                    }
                    if(mMediaPlayer != null && mMediaPlayer.isPlaying()) {
                        mMediaPlayer.pause();
                        currentNumber.setmTime(mMediaPlayer.getCurrentPosition());
                        holder.playPause.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                    } else if(mMediaPlayer != null){
                        mMediaPlayer = MediaPlayer.create(context, word.getSongResourceId());
                        mMediaPlayer.seekTo((int)currentNumber.getmTime());
                        mMediaPlayer.start();
                        mMediaPlayer.setOnCompletionListener(mCompletionListener);
                        holder.playPause.setImageResource(R.drawable.ic_pause_black_24dp);
                    }
                }
            }
        });

        holder.stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!flag && mMediaPlayer != null) {
                    mMediaPlayer.stop();
                    mMediaPlayer.release();
                    flag = true;
                }
                holder.playPause.setImageResource(R.drawable.ic_play_arrow_black_24dp);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nameTextView,singerTextView;
        ImageView image,playPause,stop;
        public MyViewHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.tv_name);
            singerTextView = (TextView) itemView.findViewById(R.id.tv_singer);
            image = (ImageView) itemView.findViewById(R.id.iv_one);
            playPause = (ImageView) itemView.findViewById(R.id.iv_play_pause);
            stop = (ImageView) itemView.findViewById(R.id.iv_stop);
        }
    }
}


