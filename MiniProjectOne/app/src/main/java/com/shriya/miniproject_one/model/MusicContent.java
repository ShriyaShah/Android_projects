package com.shriya.miniproject_one.model;

/**
 * Created by shriya on 3/1/18.
 */

public class MusicContent {

    private String songName;
    private String singer;
    private int mImageResourceid;
    private int mSongId;
    private float mTime = 0;

    public MusicContent(String name, String singer, int imageID, int songId) {
        songName = name;
        this.singer = singer;
        mImageResourceid = imageID;
        mSongId = songId;
    }

    public String getSongName() {
        return songName;
    }

    public String getSinger() {
        return singer;
    }

    public int getImageResourceId() {
        return mImageResourceid;
    }

    public int getSongResourceId() { return mSongId; }

    public float getmTime() { return mTime; }

    public void setmTime(float mTime) {
        this.mTime = mTime;
    }
}
