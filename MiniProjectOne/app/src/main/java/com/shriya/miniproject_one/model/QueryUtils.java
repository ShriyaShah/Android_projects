package com.shriya.miniproject_one.model;

import android.text.TextUtils;
import android.util.Log;

import com.shriya.miniproject_one.fragment.ListFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;

/**
 * Created by shriya on 10/1/18.
 */

public class QueryUtils {

    public static final String LOG_TAG = ListFragment.class.getSimpleName();

    public static ArrayList<JsonList> fetchListData(String requestUrl) {
    URL url = createUrl(requestUrl);
    String jsonResponse = null;
    try {
        jsonResponse = makeHttpRequest(url);
    } catch (IOException e) {
        Log.e(LOG_TAG, "Problem making the HTTP request.", e);
    }

        ArrayList<JsonList> earthquakes = extractFeatureFromJson(jsonResponse);
    return earthquakes;
}


    private static URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Problem building the URL ", e);
        }
        return url;
    }


    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";

        if (url == null) {
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error response code: " + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem retrieving the earthquake JSON results.", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

    public static ArrayList<JsonList> extractFeatureFromJson(String url) {
        if (TextUtils.isEmpty(url)) {
            return null;
        }
        ArrayList<JsonList> lists = new ArrayList<>();
        try {
            JSONArray array = new JSONArray(url);
            for(int i=0; i<array.length();i++)
            {
                JSONObject featureObject = array.getJSONObject(i);
                    String id = featureObject.getString("id");
                    String name = featureObject.getString("Name");
                    String mobile = featureObject.getString("Mobile");
                    String city = featureObject.getString("City");
                    String image = featureObject.getString("Image");
                    String date = featureObject.getString("Date");
                    String company = featureObject.getString("Company");
                    String professional = featureObject.getString("Professional");
                    String gender = featureObject.getString("Gender");

                    JsonList list = new JsonList(id, name, mobile, city, image, date, company, professional, gender);
                    lists.add(list);
            }
        } catch (JSONException e) {
            Log.e("QueryUtils", "Problem parsing the earthquake JSON results", e);
        }
        return lists;
    }

}


