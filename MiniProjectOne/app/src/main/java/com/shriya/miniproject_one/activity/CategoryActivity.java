package com.shriya.miniproject_one.activity;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.shriya.miniproject_one.fragment.ListFragment;
import com.shriya.miniproject_one.fragment.MusicFragment;
import com.shriya.miniproject_one.fragment.ProfileFragment;
import com.shriya.miniproject_one.R;

public class CategoryActivity extends AppCompatActivity implements ListFragment.OnFragmentInteractionListener{
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        Toast.makeText(getApplicationContext(),"Category Activity",Toast.LENGTH_LONG).show();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("List");
        viewPager= findViewById(R.id.pager);

        TabLayout tabLayout = findViewById(R.id.tablayout);
        tabLayout.addTab(tabLayout.newTab().setText("List").setIcon(R.drawable.ic_list_black_24dp_tab));
        tabLayout.addTab(tabLayout.newTab().setText("Music").setIcon(R.drawable.ic_play_arrow_black_24dp_tab));
        tabLayout.addTab(tabLayout.newTab().setText("Profile").setIcon(R.drawable.ic_person_black_24dp));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(mSectionsPagerAdapter);
        viewPager.setOffscreenPageLimit(2);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                switch (tab.getPosition()){
                    case 0:
                        changeTitle("List");
                        break;
                    case 1:
                        changeTitle("Music");
                        break;
                    case 2:
                        changeTitle("Profile");
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        });
    }

    @Override
    public void onFragmentInteraction(String image, String id, String name, String mobile, String city, String date, String company, String professional, String gender) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra("mimage",image);
        intent.putExtra("mid",id);
        intent.putExtra("mname",name);
        intent.putExtra("mmobile",mobile);
        intent.putExtra("mcity",city);
        intent.putExtra("mdate",date);
        intent.putExtra("mcompany",company);
        intent.putExtra("mprofessional",professional);
        intent.putExtra("mgender",gender);
        startActivity(intent);

    }

    private void changeTitle(String title){
       getSupportActionBar().setTitle(title);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {  super(fm); }

        @Override
        public Fragment getItem(int position) {
            switch (position)
            {   case 0:
                    return new ListFragment();
                case 1:
                    return new MusicFragment();
                case 2:
                    return new ProfileFragment();
            }
            return null;
        }
        @Override
        public int getCount() {
            return 3;
        }

    }
}
