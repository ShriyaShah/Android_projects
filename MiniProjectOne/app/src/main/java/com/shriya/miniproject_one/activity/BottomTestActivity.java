package com.shriya.miniproject_one.activity;

import android.app.FragmentManager;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ListFragment;

import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;;
import android.widget.Toast;
import com.shriya.miniproject_one.R;
import com.shriya.miniproject_one.fragment.MusicFragment;
import com.shriya.miniproject_one.fragment.ProfileFragment;

public class BottomTestActivity extends AppCompatActivity  {
 Intent intent = getIntent();
   // private TextView mTextMessage;
   android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
    android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            fragmentTransaction = fragmentManager.beginTransaction();

            switch (item.getItemId()) {
                case R.id.action_add_list:
                    fragmentTransaction.replace(R.id.bottom_navigation_container, new com.shriya.miniproject_one.fragment.ListFragment());

                    break;
                case R.id.action_add_music:
                    fragmentTransaction.replace(R.id.bottom_navigation_container, new MusicFragment());
                    Toast.makeText(getApplicationContext(),"Music" , Toast.LENGTH_LONG).show();
                    break;
                case R.id.action_add_profile:
                    fragmentTransaction.replace(R.id.bottom_navigation_container, new ProfileFragment());
                    Toast.makeText(getApplicationContext(),"Profile" , Toast.LENGTH_LONG).show();
                    break;
            }
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_test);

       // mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        fragmentTransaction.replace(R.id.bottom_navigation_container, new com.shriya.miniproject_one.fragment.ListFragment()).commit();
    }

//    @Override
//    public void onFragmentInteraction(String image, String id, String name, String mobile, String city, String date, String company, String professional, String gender) {
//        Intent intent = new Intent(this, DetailsActivity.class);
//        intent.putExtra("mimage",image);
//        intent.putExtra("mid",id);
//        intent.putExtra("mname",name);
//        intent.putExtra("mmobile",mobile);
//        intent.putExtra("mcity",city);
//        intent.putExtra("mdate",date);
//        intent.putExtra("mcompany",company);
//        intent.putExtra("mprofessional",professional);
//        intent.putExtra("mgender",gender);
//        startActivity(intent);
//    }
}
