package com.shriya.miwok;


import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhrasesFragment extends Fragment {
    private MediaPlayer mMediaPlayer;
    private AudioManager mAudioManager;
    private AudioManager.OnAudioFocusChangeListener mOnAudioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int focusChange) {
            if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT ||
                    focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                mMediaPlayer.pause();
                mMediaPlayer.seekTo(0);
            } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                mMediaPlayer.start();
            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                releaseMediaPlayer();
            }
        }
    };
    private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            releaseMediaPlayer();
        }
    };
    public PhrasesFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_phrases, container, false);
        mAudioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        final ArrayList<Word> words = new ArrayList<Word>();
        words.add(new Word("one","lutti", R.raw.phrase_are_you_coming));
        words.add(new Word("two","lutti2", R.raw.phrase_are_you_coming));
        words.add(new Word("three","lutti3", R.raw.phrase_are_you_coming));
        words.add(new Word("four","lutti4", R.raw.phrase_are_you_coming));
        words.add(new Word("five","lutti5", R.raw.phrase_are_you_coming));
        words.add(new Word("six","lutti6", R.raw.phrase_are_you_coming));
        words.add(new Word("seven","lutti7", R.raw.phrase_are_you_coming));
        words.add(new Word("eight","lutti8", R.raw.phrase_are_you_coming));
        words.add(new Word("nine","lutti9", R.raw.phrase_are_you_coming));
        words.add(new Word("ten","lutti10", R.raw.phrase_are_you_coming));
        words.add(new Word("one","lutti", R.raw.phrase_are_you_coming));
        words.add(new Word("two","lutti2", R.raw.phrase_are_you_coming));
        words.add(new Word("three","lutti3", R.raw.phrase_are_you_coming));
        words.add(new Word("four","lutti4", R.raw.phrase_are_you_coming));
        words.add(new Word("five","lutti5", R.raw.phrase_are_you_coming));
        words.add(new Word("six","lutti6", R.raw.phrase_are_you_coming));

        WordAdapter adapter = new WordAdapter(getActivity(), words, R.color.category_phrases);
        GridView listView = (GridView) rootView.findViewById(R.id.list_phrases);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                releaseMediaPlayer();
                Word word = words.get(position);
                int result = mAudioManager.requestAudioFocus(mOnAudioFocusChangeListener,
                        AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    mMediaPlayer = MediaPlayer.create(getActivity(), word.getSongResourceId());
                    mMediaPlayer.start();
                    mMediaPlayer.setOnCompletionListener(mCompletionListener);
                }
            }
        });
        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
        releaseMediaPlayer();
    }
    private void releaseMediaPlayer() {
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
            mAudioManager.abandonAudioFocus(mOnAudioFocusChangeListener);
        }
    }
}
