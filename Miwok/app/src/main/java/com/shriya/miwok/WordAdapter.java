package com.shriya.miwok;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

public class WordAdapter extends ArrayAdapter<Word> {
    private int colourValue;
    public WordAdapter(Activity context, ArrayList<Word> words, int colour) {
        super(context, 0, words);
        colourValue = colour;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }
        Word currentNumber = getItem(position);
        TextView miwokTextView = (TextView) listItemView.findViewById(R.id.tv_miwok);
        miwokTextView.setText(currentNumber.getMiwokTranslation());
        miwokTextView.setTypeface(null, Typeface.BOLD_ITALIC);
        TextView numberTextView = (TextView) listItemView.findViewById(R.id.tv_default);
        numberTextView.setText(currentNumber.getDefaultTranslation());
        numberTextView.setTypeface(null, Typeface.ITALIC);
        ImageView image = (ImageView) listItemView.findViewById(R.id.iv_one);
        if(currentNumber.hasImage()){
            image.setImageResource(currentNumber.getImageResourceId());
            image.setVisibility(View.VISIBLE);
        }

        else {
            image.setVisibility(View.GONE);
        }
        View textContainer = listItemView.findViewById(R.id.text_container);
                         int color = ContextCompat.getColor(getContext(), colourValue);
                         textContainer.setBackgroundColor(color);
        return listItemView;
    }
}
