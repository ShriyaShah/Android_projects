package com.shriya.miwok;

public class Word {

    private String mDefaultTranslation;
    private String mMiwokTranslation;
    private final int noImage = -1;
    private int mImageResourceid = noImage;
    private int mSongId;

    public Word(String defaultTranslation, String miwokTranslation, int songId) {
        mDefaultTranslation = defaultTranslation;
        mMiwokTranslation = miwokTranslation;
        mSongId = songId;
    }

    public Word(String defaultTranslation, String miwokTranslation, int imageID, int songId) {
        mDefaultTranslation = defaultTranslation;
        mMiwokTranslation = miwokTranslation;
        mImageResourceid = imageID;
        mSongId = songId;
    }

    public String getDefaultTranslation() {
        return mDefaultTranslation;
    }

    public String getMiwokTranslation() {
        return mMiwokTranslation;
    }

    public int getImageResourceId() {
        return mImageResourceid;
    }

    public boolean hasImage() { return mImageResourceid != noImage; }

    public int getSongResourceId() { return mSongId; }
}