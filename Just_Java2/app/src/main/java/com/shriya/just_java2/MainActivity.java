package com.shriya.just_java2;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText quantityEditText, nameView;
    TextView price;
    Button increment, decrement, order;
    CheckBox cream, chocolate;
    int number, orderPrice;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        quantityEditText = (EditText) findViewById(R.id.tv_quantity_text_view);
        //quantityEditText.setKeyListener(null);
        nameView = (EditText) findViewById(R.id.et_name);
      //  price = (TextView) findViewById(R.id.price_text_view);
        increment = (Button) findViewById(R.id.bt_increasevalue);
        decrement = (Button) findViewById(R.id.bt_decreasevalue);
        order = (Button) findViewById(R.id.order_button);
        cream = (CheckBox) findViewById(R.id.whipped_cream);
        chocolate = (CheckBox) findViewById(R.id.chocolate);

        increment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = Integer.parseInt(quantityEditText.getText().toString());
                displayQuantity(++number);
            }
        });

        decrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                number = Integer.parseInt(quantityEditText.getText().toString());
                number = number - 1;
                if ((int) number < 0 )
                    number = 0;
                displayQuantity(number);
            }
        });

        order.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int wc = 0, c = 0;
                if (cream.isChecked())
                    wc = 10;
                if (chocolate.isChecked())
                    c = 20;
                if(quantityEditText.getText().toString().isEmpty())
                {
                    Toast.makeText(getApplicationContext(),"Enter quantity!", Toast.LENGTH_LONG).show();
                }
                else {
                    number = Integer.parseInt(quantityEditText.getText().toString());
                    orderPrice = number * 50 + wc * number + c * number;
                    displayPrice();
                    Toast.makeText(getApplicationContext(), "Your total amount is Rs." + orderPrice, Toast.LENGTH_SHORT).show();
                }}
        });
    }
    private void displayQuantity(int number) {
        quantityEditText.setText("" + number);
    }

    private void displayPrice() {
        String message = "Name : " + nameView.getText().toString() + "\nQuantity : " + number + "\nIs Whipped cream added? : " + cream.isChecked() + "\nIs Chocolate added : " + chocolate.isChecked() + "\nPrice : " + orderPrice + "Rs." + "\nThank you.";
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
       // intent.putExtra(Intent.EXTRA_EMAIL,);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Coffee order for " + nameView.getText().toString());
        intent.putExtra(Intent.EXTRA_TEXT, message);

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }

//        price.setText("Name : " + nameView.getText().toString() + "\nQuantity : " + number + "\nIs Whipped cream added? : " + cream.isChecked() + "\nIs Chocolate added : " + chocolate.isChecked() + "\nPrice : " + orderPrice + "Rs." + "\nThank you.");
    }
}
