package com.shriya.getbroadcast;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    public static final String CUSTOM_INTENT = "my.custom.action.tag.fordemo";
    Button buttonBroadcastIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonBroadcastIntent = (Button) findViewById(R.id.button);
        buttonBroadcastIntent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(CUSTOM_INTENT);
                sendBroadcast(intent);
            }
        });


    }
}
