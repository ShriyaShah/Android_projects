package com.shriya.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.Collections;
import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder> {
    Context context ;
    List<Information> data = Collections.emptyList();
    private LayoutInflater inflator;
    public ListAdapter(Context context, List<Information> data)
    {
        this.context=context;
        this.data = data;
    }
    @Override
    public ListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflator = LayoutInflater.from(context);
        View view = inflator.inflate(R.layout.custom_view,  parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ListAdapter.MyViewHolder holder, int position) {
        Information current = data.get(position);
        holder.value.setText(current.item_value);
        holder.imageId.setImageResource(current.Image_id);
        //int a = getItemCount();
        // Log.v("Size","" + a );
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageId;
        TextView value;
        public MyViewHolder(View itemView) {
            super(itemView);
            imageId = (ImageView) itemView.findViewById(R.id.iv_icon);
            value = (TextView) itemView.findViewById(R.id.tv_text);
        }
    }
}

