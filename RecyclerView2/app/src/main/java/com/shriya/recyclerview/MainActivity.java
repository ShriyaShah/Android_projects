package com.shriya.recyclerview;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        ListAdapter listAdapter = new ListAdapter(this, getData());
        recyclerView.setAdapter(listAdapter);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(4,StaggeredGridLayoutManager.VERTICAL));
    }
    public static List<Information> getData() {
        List<Information> data = new ArrayList<>();
        int[] icons = {R.drawable.one, R.drawable.one, R.drawable.ic_launcher_background, R.drawable.one, R.drawable.one, R.drawable.one};
        String[] values = {"One", "Two", "Three", "Four", "Five", "Six"};
        for (int i = 0; i < icons.length && i < values.length; i++) {
            Information current = new Information();
            current.Image_id = icons[i];
            current.item_value = values[i];
            data.add(current);

        } return data;
    }

}
