package com.shriya.progressdialog;

import android.app.ProgressDialog;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button download;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        download = (Button) findViewById(R.id.button);
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog = new ProgressDialog(MainActivity.this);
                progressDialog.setProgress(0);
                progressDialog.setMax(100);
                progressDialog.setTitle("Download");
                progressDialog.setMessage("It is loading.....");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progressDialog.show();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try
                        {
                            while(progressDialog.getProgress() <= progressDialog.getMax())
                            {
                                Thread.sleep(200);
                                hanlde.sendMessage(hanlde.obtainMessage());
                                if(progressDialog.getProgress() == progressDialog.getMax())
                                {
                                    progressDialog.dismiss();
                                }
                            }
                        }
                        catch (Exception e){ e.printStackTrace();}

                    }
                }).start();
            }

            Handler hanlde = new Handler(){
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    progressDialog.incrementProgressBy(1);
                }
            };
        });
    }
}
